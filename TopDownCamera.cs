﻿using GTA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace TopDownCamera
{
    public class TopDownCamera : Script
    {
        private class PosRot
        {
            internal Vector3 Position, Rotation;

            internal PosRot(Vector3 pos, Vector3 rot)
            {
                Position = pos;
                Rotation = rot;
            }
        }

        private const string S_TOGGLE_KEYS = "toggle_keys", S_FOV_KEYS = "fov_keys", S_USE_CUSTOM_CONTROLS = "use_custom_controls", S_USE_HOLD_KEY = "use_hold_key", S_HOLD_KEY = "hold_key", S_PRESS_KEY = "pressKey", S_INCREASEFOV_USE_HOLD_KEY = "increasefov_use_hold_key", S_INCREASEFOV_HOLD_KEY = "increasefov_hold_key", S_INCREASEFOV_PRESS_KEY = "increasefov_press_key", S_DECREASEFOV_USE_HOLD_KEY = "decreasefov_use_hold_key", S_DECREASEFOV_HOLD_KEY = "decreasefov_hold_key", S_DECREASE_PRESS_KEY = "decreasefov_press_key";
        private const bool DEFAULT_USE_CUSTOM_CONTROLS = false, DEFAULT_USE_HOLD_KEY = true, DEFAULT_INCREASEFOV_USE_HOLD_KEY = true, DEFAULT_DECREASEFOV_USE_HOLD_KEY = true;
        private const Keys DEFAULT_HOLD_KEY = Keys.RControlKey, DEFAULT_PRESS_KEY = Keys.V, DEFAULT_INCREASEFOV_HOLD_KEY = Keys.RControlKey, DEFAULT_INCREASEFOV_PRESS_KEY = Keys.Oemplus, DEFAULT_DECREASEFOV_HOLD_KEY = Keys.RControlKey, DEFAULT_DECREASEFOV_PRESS_KEY = Keys.OemMinus;

        private bool useCustomControls, useHoldKey, increasefovUseHoldKey, decreasefovUseHoldKey;
        private Keys holdKey, pressKey, increasefovHoldKey, increasefovPressKey, decreasefovHoldKey, decreasefovPressKey;

        private Camera cam = null;
        private float zOffset = 25;
        private double lastTimePressedV = 0;
        private bool died = false;

        public TopDownCamera()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue(S_USE_CUSTOM_CONTROLS, S_TOGGLE_KEYS, DEFAULT_USE_CUSTOM_CONTROLS);
                //---
                Settings.SetValue(S_HOLD_KEY, S_TOGGLE_KEYS, DEFAULT_HOLD_KEY);
                Settings.SetValue(S_USE_HOLD_KEY, S_TOGGLE_KEYS, DEFAULT_USE_HOLD_KEY);
                Settings.SetValue(S_PRESS_KEY, S_TOGGLE_KEYS, DEFAULT_PRESS_KEY);
                //---
                Settings.SetValue(S_INCREASEFOV_USE_HOLD_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_USE_HOLD_KEY);
                Settings.SetValue(S_INCREASEFOV_HOLD_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_HOLD_KEY);
                Settings.SetValue(S_INCREASEFOV_PRESS_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_PRESS_KEY);
                //---
                Settings.SetValue(S_DECREASEFOV_USE_HOLD_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_USE_HOLD_KEY);
                Settings.SetValue(S_DECREASEFOV_HOLD_KEY, S_FOV_KEYS, DEFAULT_DECREASEFOV_HOLD_KEY);
                Settings.SetValue(S_DECREASE_PRESS_KEY, S_FOV_KEYS, DEFAULT_DECREASEFOV_PRESS_KEY);
                Settings.Save();
            }

            useCustomControls = Settings.GetValueBool(S_USE_CUSTOM_CONTROLS, S_TOGGLE_KEYS, DEFAULT_USE_CUSTOM_CONTROLS);
            //---
            holdKey = Settings.GetValueKey(S_HOLD_KEY, S_TOGGLE_KEYS, DEFAULT_HOLD_KEY);
            useHoldKey = Settings.GetValueBool(S_USE_HOLD_KEY, S_TOGGLE_KEYS, DEFAULT_USE_HOLD_KEY);
            pressKey = Settings.GetValueKey(S_PRESS_KEY, S_TOGGLE_KEYS, DEFAULT_PRESS_KEY);
            //---
            increasefovUseHoldKey = Settings.GetValueBool(S_INCREASEFOV_USE_HOLD_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_USE_HOLD_KEY);
            increasefovHoldKey = Settings.GetValueKey(S_INCREASEFOV_HOLD_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_HOLD_KEY);
            increasefovPressKey = Settings.GetValueKey(S_INCREASEFOV_PRESS_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_PRESS_KEY);
            //---
            decreasefovUseHoldKey = Settings.GetValueBool(S_DECREASEFOV_USE_HOLD_KEY, S_FOV_KEYS, DEFAULT_INCREASEFOV_USE_HOLD_KEY);
            decreasefovHoldKey = Settings.GetValueKey(S_DECREASEFOV_HOLD_KEY, S_FOV_KEYS, DEFAULT_DECREASEFOV_HOLD_KEY);
            decreasefovPressKey = Settings.GetValueKey(S_DECREASE_PRESS_KEY, S_FOV_KEYS, DEFAULT_DECREASEFOV_PRESS_KEY);

            KeyDown += TopDownCamera_KeyDown;
            Interval = 50;
            Tick += TopDownCamera_Tick;
        }

        private void TopDownCamera_Tick(object sender, EventArgs e)
        {
            if (cam != null)
            {
                if (Player.Character.isDead) died = true;
                else if (died)
                {
                    died = false;
                    if (cam != null)
                    {
                        AttachCamToPed(cam, Player.Character);
                        SetCamAttachOffset(cam, new Vector3(0, 0, zOffset));
                    }
                    else ActivateCam();
                }

                if ((!increasefovUseHoldKey || isKeyPressed(increasefovHoldKey)) && isKeyPressed(increasefovPressKey))
                {
                    zOffset += 0.3f;
                    SetCamAttachOffset(cam, new Vector3(0, 0, zOffset));
                }
                else if ((!decreasefovUseHoldKey || isKeyPressed(decreasefovHoldKey)) && isKeyPressed(decreasefovPressKey) && zOffset >= 2f)
                {
                    zOffset -= 0.3f;
                    SetCamAttachOffset(cam, new Vector3(0, 0, zOffset));
                }
            }
        }

        private void TopDownCamera_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (useCustomControls)
            {
                if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey)
                {
                    if (cam == null) ActivateCam();
                    else DisableCam();
                }
            }
            else if (e.Key == pressKey)
            {
                double now = DateTime.Now.TimeOfDay.TotalMilliseconds;
                if (Math.Abs(now - lastTimePressedV) < 750) ActivateCam();
                else if (cam != null) DisableCam();
                lastTimePressedV = now;
            }
        }

        private void ActivateCam()
        {
            if (cam != null) DisableCam();

            cam = new Camera();
            cam.Activate();
            AttachCamToPed(cam, Player.Character);
            SetCamAttachOffset(cam, new Vector3(0, 0, zOffset));
            cam.LookAt(Player.Character);
        }

        private void DisableCam()
        {
            if (cam.isActive) cam.Deactivate();
            else cam.Delete();
            cam = null;
        }

        /// <param name="startPosition">X = left/right, Y = front/backwards, Z = up/down</param>
        /// <param name="startRotation">X = pitch (nose-up-back-down/nose-down-back-up), Y = roll, Z = heading</param>
        private PosRot[] GetAnimationPositions(Vector3 startPosition, Vector3 startRotation)
        {
            List<PosRot> posAndRots = new List<PosRot>();
            posAndRots.Add(new PosRot(VecGetOffset(startPosition, new Vector3(0, -1, 0)), startRotation));

            return posAndRots.ToArray();
        }

        private Vector3 VecGetOffset(Vector3 vec, Vector3 offset)
        {
            return vec + offset;
        }

        private void AttachCamToPed(Camera cam, Ped ped)
        {
            GTA.Native.Function.Call("ATTACH_CAM_TO_PED", cam, ped);
        }

        private void SetCamAttachOffset(Camera cam, Vector3 offset)
        {
            GTA.Native.Function.Call("SET_CAM_ATTACH_OFFSET", cam, offset.X, offset.Y, offset.Z);
        }
    }
}
