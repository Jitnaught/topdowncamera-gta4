Have you ever wanted to play GTA IV like in the olden days of GTA I and II? Well then this is the mod for you!

How to install:
Install an ASI Loader (YAASIL recommended) and .NET Scripthook.
Copy TopDownCamera.net.dll and TopDownCamera.ini to the scripts folder in your GTA IV/EFLC directory.

Controls:
Press V twice (the second press within 3/4 of a second of the first press) to toggle the top down view camera
Hold Right-control + the plus button to increase the FOV of the top down camera
Hold Right-control + the minus button to decrease the FOV of the top down camera 

What you can change in the INI file:
The control to toggle the top down camera (you can make it either double-press-V or a custom key combo like Right-control + V*)
The controls to increase the FOV of the top down camera
The controls to decrease the FOV of the top down camera
*To make it so you can use a custom key combo instead of the default double-press-V, you have to change the "use_custom_controls" value to "True", then change the actual keys

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
